FROM mysql:5.7.23

ENV MYSQL_ROOT_PASSWORD=local
ENV MYSQL_DATABASE=local
ENV MYSQL_USER=local
ENV MYSQL_PASSWORD=local

RUN mkdir /scripts
COPY ./scripts/V* /scripts

WORKDIR /scripts
# Sort and concat version scripts
RUN cat $(find ./ -name "V*" | sort -V) > init.sql
RUN mv init.sql /docker-entrypoint-initdb.d/

# Add database settings
RUN echo '[client]' >> /etc/mysql/my.cnf
RUN echo 'default-character-set=utf8mb4' >> /etc/mysql/my.cnf
RUN echo '[mysql]' >> /etc/mysql/my.cnf
RUN echo 'default-character-set=utf8mb4' >> /etc/mysql/my.cnf
RUN echo '[mysqld]' >> /etc/mysql/my.cnf
RUN echo 'collation-server = utf8mb4_unicode_ci' >> /etc/mysql/my.cnf
RUN echo "init-connect='SET NAMES utf8mb4'" >> /etc/mysql/my.cnf
RUN echo 'character-set-server = utf8mb4' >> /etc/mysql/my.cnf