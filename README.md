# Template de configuração de um banco MySQL utilizando Docker
A configuração manual de um banco localmente costuma demorar um pouquinho e muitas vezes você acaba instalando mais coisas do que precisava. Depois que passei a configurar localmente bancos de dados utilizando Docker, minha ficou muito mais fácil.
Dentre os benefícios de usar essa configuração, vale ressaltar:
* Fácil e rápido de configurar;
* Mais fácil de compartilhar a configuração com outros desenvolvedores do time, principalmente os menos experientes;
* Pode ser útil para efetuar testes de integração, bastando subir um banco de teste;
* Facilita a manutenção de vários bancos em uma só máquina, sendo possível parar/iniciar o container conforme a necessidade;

**É importante ressaltar que esse tipo de configuração é mais indicado para uso local, não cheguei a testar em ambientes de produção.**

## Pré-requisitos
* Docker instalado [link para download](https://www.docker.com/products/docker-desktop).

## Configuração
Adicione o arquivo Dockerfile. Dentro do arquivo defina o valor para as seguintes variáveis:
* MYSQL_ROOT_PASSWORD: senha do root;
* MYSQL_DATABASE: nome do banco a ser criado;
* MYSQL_USER: nome do usuário a ser criado;
* MYSQL_PASSWORD: senha do usuário a ser criado.

Dentro do Dockerfile eu adicionei um comando para ordenar os scripts V e concatená-los em um só arquivo (padrão utilizando para versionamento de banco com o [Flyway](https://flywaydb.org/)).

Também adicionei algumas configurações de charset e collation no banco. Aqui você pode fazer outras configurações que julgar necessário.

Adicione os scripts de inicialização do banco dentro da pasta scripts.

Para criar a imagem, execute o seguinte comando na raiz do repositório:
```
docker build -t <nome_da_imagem> .
```

Para criar um container a partir da imagem, execute o seguinte comando:
```
docker run --name <nome_container> -p <porta_local>:3306 <nome_imagem>
```

Para parar container execute:
```
docker stop <nome_container>
```

Para iniciar o container execute:
```
docker start <nome_container>
```